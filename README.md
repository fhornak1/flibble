# Flibble
**Whou would clean up the mess?**

```scala


val skewedStream: DataStream[Foo] = ???

val countOfKeys = skewedStream
  .uniformizedBy(_.random)
  .window(TumblingTimeWindow.of(Time.seconds(5)))
  .keyBy(_.skewedKey)
  .window(TumblingTimeWindow.of(Time.minutes(5)))
  .sum

```