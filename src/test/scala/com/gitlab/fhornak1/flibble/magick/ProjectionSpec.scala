package com.gitlab.fhornak1.flibble.magick

import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class ProjectionSpec extends AnyWordSpec with Matchers {

  "Projection" should {

    "Use identity map" in {
      val a = Set(1, 2, 3)

      val b = Projection.mapTo[Set[Int], Set[Int]](a)

      b should equal(Set(1, 2, 3))  // Set is not a Product type

    }

    "properly derive cast from Foo to Bar" in {
      case class Foo(x: Int, y: Boolean, z: Double)

      case class Bar(z: Double, x: Int)

      val bar: Bar = Projection.mapTo[Foo, Bar](Foo(1, true, 2.0))

      bar should equal(Bar(2.0, 1))
    }

    "project into HList with labeled types" in {
      import shapeless._
      import shapeless.syntax.singleton._
      import shapeless.record._

      case class Foo(foo: String, bar: Boolean, baz: Long)

      val b = Foo("abc", false, 12L)
      val res = ("foo" ->> "abc") :: ("bar" ->> false) :: ("baz" ->> 12L) :: HNil

      val br = LabelledGeneric[Foo].to(b)

      println(br.fieldAt('foo))

      br should equal(res)
    }
  }
}
