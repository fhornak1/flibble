package com.gitlab.fhornak1.flibble.magick

import shapeless._

trait FromCaseClassMap[Source, TargetHList] {
  def apply(source: Source): TargetHList
}

object FromCaseClassMap {

  implicit def fromCaseClassMap[Source, SourceRepr <: HList, TargetRepr <: HList](implicit
      genericSource: Generic.Aux[Source, SourceRepr],
      pr: Projection[SourceRepr, TargetRepr]
  ): FromCaseClassMap[Source, TargetRepr] =
    (source: Source) => pr(genericSource.to(source))
}
