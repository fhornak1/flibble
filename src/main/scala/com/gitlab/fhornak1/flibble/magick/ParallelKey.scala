package com.gitlab.fhornak1.flibble.magick

import org.apache.flink.api.java.functions.KeySelector

trait ParallelKey[I] extends Serializable {
  type Out

  def apply(in: I): Out

  def selector: KeySelector[I, Out] = (value: I) => apply(value)
}

object ParallelKey {

  type Aux[I, O] =
    ParallelKey[I] {
      type Out = O
    }

  def apply[T](implicit ev: ParallelKey[T]): ParallelKey[T] = ev
}
