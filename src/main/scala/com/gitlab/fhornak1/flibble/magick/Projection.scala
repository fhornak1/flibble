package com.gitlab.fhornak1.flibble.magick

import shapeless._
import shapeless.ops.hlist.{ Align, Intersection }

trait Projection[S, T] {
  def apply(source: S): T
}

object Projection {

  def mapTo[P, T](value: P)(implicit pr: Projection[P, T]): T = pr(value)

  def apply[S, T](implicit ev: Projection[S, T]): Projection[S, T] = ev

  implicit def mkIdProj[A]: Projection[A, A] = identity

  implicit def mkNilProj[L <: HList]: Projection[L, HNil] = (_: L) => HNil

  implicit def mkHListHListProj[LRepr <: HList, TRepr <: HList, Unaligned <: HList](
    implicit
    intersect: Intersection.Aux[LRepr, TRepr, Unaligned],
    align: Align[Unaligned, TRepr]
  ): Projection[LRepr, TRepr] = (source: LRepr) => align(intersect(source))

  implicit def mkProdHListProj[P <: Product, PRepr <: HList, TRepr <: HList](
    implicit
    pGen: LabelledGeneric.Aux[P, PRepr],
    pr: Projection[PRepr, TRepr]
  ): Projection[P, TRepr] = (source: P) => pr(pGen.to(source))

  implicit def mkHListProdProj[PRepr <: HList, T <: Product, TRepr <: HList](
    implicit
    tGen: LabelledGeneric.Aux[T, TRepr],
    pr: Projection[PRepr, TRepr]
  ): Projection[PRepr, T] = (source: PRepr) => tGen.from(pr(source))

  implicit def mkProdProdProj[P <: Product, PRepr <: HList, T <: Product, TRepr <: HList](
    implicit
    pGen: LabelledGeneric.Aux[P, PRepr],
    tGen: LabelledGeneric.Aux[T, TRepr],
    pr: Projection[PRepr, TRepr]
  ): Projection[P, T] = (source: P) => tGen.from(pr(pGen.to(source)))
}
