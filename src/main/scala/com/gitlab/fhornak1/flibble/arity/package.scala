package com.gitlab.fhornak1.flibble

import cats.Semigroup
import shapeless._

package object arity {

  object magick {

    implicit def mkProdSemi[P <: Product, PRepr <: HList](
      implicit
      gen: Generic.Aux[P, PRepr],
      semi: Semigroup[PRepr]
    ): Semigroup[P] = (x: P, y: P) => gen.from(semi.combine(gen.to(x), gen.to(y)))

    implicit def mkSemi[A, HH <: HList](
      implicit
      semiA: Semigroup[A],
      semiHH: Semigroup[HH]
    ): Semigroup[A :: HH] =
      (x: A :: HH, y: A :: HH) => semiA.combine(x.head, y.head) :: semiHH.combine(x.tail, y.tail)

    implicit def mkSemiSingle[A](implicit semiA: Semigroup[A]): Semigroup[A :: HNil] =
      (x: A :: HNil, y: A :: HNil) => semiA.combine(x.head, y.head) :: HNil

    implicit val mkSemiHNil: Semigroup[HNil] = (x: HNil, y: HNil) => HNil

  }

}
