package com.gitlab.fhornak1.flibble.multilevel

import org.apache.flink.api.common.functions.AggregateFunction

class SecondStageAggregator[K, ACC, O](agg: AggregateFunction[_, ACC, O])
    extends AggregateFunction[(K, ACC), ACC, O] {
  override def createAccumulator(): ACC = agg.createAccumulator()

  override def add(
    value: (K, ACC),
    accumulator: ACC
  ): ACC = agg.merge(value._2, accumulator)

  override def getResult(accumulator: ACC): O = agg.getResult(accumulator)

  override def merge(
    a: ACC,
    b: ACC
  ): ACC = agg.merge(a, b)
}
