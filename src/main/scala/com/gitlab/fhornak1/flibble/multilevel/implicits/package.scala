package com.gitlab.fhornak1.flibble.multilevel

import org.apache.flink.api.common.typeinfo.TypeInformation
import org.apache.flink.api.java.functions.KeySelector
import org.apache.flink.streaming.api.scala.DataStream

package object implicits {

  implicit class MultiStageAggregator[T](val ds: DataStream[T]) extends AnyVal {

    def uniformizeBy[U : TypeInformation](uni: T => U): Unit =
      uniformizeBy(
        new KeySelector[T, U] {
          override def getKey(value: T): U = uni(value)
        })

    def uniformizeBy[U : TypeInformation](uni: KeySelector[T, U]): UniformWindowSelector[T, U] =
      new UniformWindowSelector(ds, uni)

  }

}
