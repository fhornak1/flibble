package com.gitlab.fhornak1.flibble.multilevel

import org.apache.flink.api.common.typeinfo.TypeInformation
import org.apache.flink.api.java.functions.KeySelector
import org.apache.flink.streaming.api.scala.DataStream
import org.apache.flink.streaming.api.windowing.assigners.{TumblingEventTimeWindows, WindowAssigner}
import org.apache.flink.streaming.api.windowing.windows.Window
/*
  stream
    .uniformize(_.myUniformlyDistKey)
    .window(TumblingEventTimeWindows.of(...))
    .keyBy(_.key)
    .window(TumblingEventTimeWindows.of(...))
    .aggregate(...)
 */

class WindowSelectorBuilder[T : TypeInformation, U : TypeInformation, W <: Window, K : TypeInformation](
  ds: DataStream[T],
  uni: KeySelector[T, U],
  sw: WindowAssigner[_, W],
  ks: KeySelector[T, K]) {

  def window(wnd: WindowAssigner[_, W]): AggregationPipelineBuilder[T, U, W, K] =
    new AggregationPipelineBuilder[T, U, W, K](ds, uni, sw, ks, wnd)
}
