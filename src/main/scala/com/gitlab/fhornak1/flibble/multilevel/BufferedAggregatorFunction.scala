package com.gitlab.fhornak1.flibble.multilevel

import org.apache.flink.api.common.functions.{ AggregateFunction, RichAggregateFunction }
import org.apache.flink.api.common.state.{ MapState, MapStateDescriptor }
import org.apache.flink.api.java.functions.KeySelector
import org.apache.flink.configuration.Configuration

import scala.collection.mutable

class BufferedAggregatorFunction[InT, AccT, OutT, K](
  selector: KeySelector[InT, K],
  agg: AggregateFunction[InT, AccT, OutT])
    extends AggregateFunction[InT, mutable.Map[K, AccT], Map[K, AccT]] {

  override def createAccumulator(): mutable.Map[K, AccT] = mutable.Map()

  override def add(
    value: InT,
    accumulator: mutable.Map[K, AccT]
  ): mutable.Map[K, AccT] = {
    val key = selector.getKey(value)
    accumulator += key -> agg.add(value, accumulator.getOrElse(key, agg.createAccumulator()))
  }

  override def getResult(accumulator: mutable.Map[K, AccT]): Map[K, AccT] = accumulator.toMap

  override def merge(
    a: mutable.Map[K, AccT],
    b: mutable.Map[K, AccT]
  ): mutable.Map[K, AccT] = ???
}
