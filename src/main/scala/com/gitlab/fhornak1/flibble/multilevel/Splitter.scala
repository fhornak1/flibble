package com.gitlab.fhornak1.flibble.multilevel

import org.apache.flink.api.common.functions.FlatMapFunction
import org.apache.flink.util.Collector

class Splitter[K, ACC] extends FlatMapFunction[Map[K, ACC], (K, ACC)] {

  override def flatMap(
    value: Map[K, ACC],
    out: Collector[(K, ACC)]
  ): Unit =
    value.foreach(out.collect)
}
