package com.gitlab.fhornak1.flibble.multilevel

import org.apache.flink.api.common.functions.AggregateFunction
import org.apache.flink.api.common.typeinfo.TypeInformation
import org.apache.flink.api.java.functions.KeySelector
import org.apache.flink.api.scala._
import org.apache.flink.streaming.api.scala.DataStream
import org.apache.flink.streaming.api.windowing.assigners.WindowAssigner
import org.apache.flink.streaming.api.windowing.windows.Window

class AggregationPipelineBuilder[
  T : TypeInformation,
  U : TypeInformation,
  W <: Window,
  K : TypeInformation
](
  ds: DataStream[T],
  unif: KeySelector[T, U],
  shortWindow: WindowAssigner[_, W],
  key: KeySelector[T, K],
  window: WindowAssigner[_, W]) {

  def aggregate[ACC, O](agg: AggregateFunction[T, ACC, O]): DataStream[O] =
    ds.keyBy(unif)
      .window(shortWindow.asInstanceOf[WindowAssigner[T, W]])
      .aggregate(new BufferedAggregatorFunction(key, agg))
      .flatMap(new Splitter[K, ACC])
      .keyBy(_._1)
      .window(window.asInstanceOf[WindowAssigner[(K, ACC), W]])
      .aggregate(new SecondStageAggregator[K, ACC, O](agg))

  def sum[F : TypeInformation](fn: T => F): DataStream[F] = ???

  def count: DataStream[Long] = ???

  def unique[F : TypeInformation](fn: T => F): DataStream[F] = ???

  def avg[F : TypeInformation](fn: T => F): DataStream[F] = ???
}
