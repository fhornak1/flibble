package com.gitlab.fhornak1.flibble.multilevel

import org.apache.flink.api.common.typeinfo.TypeInformation
import org.apache.flink.api.java.functions.KeySelector
import org.apache.flink.streaming.api.scala.DataStream
import org.apache.flink.streaming.api.windowing.assigners.WindowAssigner
import org.apache.flink.streaming.api.windowing.windows.Window

class UniformWindowSelector[T : TypeInformation, U : TypeInformation](
  ds: DataStream[T],
  uni: KeySelector[T, U]) {

  def window[W <: Window](wnd: WindowAssigner[_, W]): UniformizedKeySelector[T, U, W] =
    new UniformizedKeySelector(ds, uni, wnd)
}
