package com.gitlab.fhornak1.flibble.multilevel

import org.apache.flink.api.common.typeinfo.TypeInformation
import org.apache.flink.api.java.functions.KeySelector
import org.apache.flink.streaming.api.scala.DataStream
import org.apache.flink.streaming.api.windowing.assigners.WindowAssigner
import org.apache.flink.streaming.api.windowing.windows.Window

class UniformizedKeySelector[T : TypeInformation, U : TypeInformation, W <: Window](
  ds: DataStream[T],
  uni: KeySelector[T, U],
  wnd: WindowAssigner[_, W]) {

  def keyBy[K : TypeInformation](keySel: T => K): Unit =
    keyBy(
      new KeySelector[T, K] {
        override def getKey(value: T): K = keySel(value)
      })

  def keyBy[K : TypeInformation](ks: KeySelector[T, K]): WindowSelectorBuilder[T, U, W, K] =
    new WindowSelectorBuilder[T, U, W, K](ds, uni, wnd, ks)
}
